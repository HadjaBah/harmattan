<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210908125015 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commande CHANGE nom_client_id nom_client_id INT DEFAULT NULL, CHANGE nom_libraire_id nom_libraire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE livres CHANGE email_client_id email_client_id INT DEFAULT NULL, CHANGE nom_org_ext_id nom_org_ext_id INT DEFAULT NULL, CHANGE nom_presse_id nom_presse_id INT DEFAULT NULL, CHANGE nom_etudiant_id nom_etudiant_id INT DEFAULT NULL, CHANGE nom_libraire_id nom_libraire_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE commande CHANGE nom_client_id nom_client_id INT DEFAULT NULL, CHANGE nom_libraire_id nom_libraire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE livres CHANGE email_client_id email_client_id INT DEFAULT NULL, CHANGE nom_org_ext_id nom_org_ext_id INT DEFAULT NULL, CHANGE nom_presse_id nom_presse_id INT DEFAULT NULL, CHANGE nom_etudiant_id nom_etudiant_id INT DEFAULT NULL, CHANGE nom_libraire_id nom_libraire_id INT DEFAULT NULL');
    }
}
