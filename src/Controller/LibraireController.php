<?php

namespace App\Controller;

use App\Entity\Libraire;
use App\Form\Libraire1Type;
use App\Repository\LibraireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/libraire")
 */
class LibraireController extends AbstractController
{
    /**
     * @Route("/", name="libraire_index", methods={"GET"})
     */
    public function index(LibraireRepository $libraireRepository): Response
    {
        return $this->render('libraire/index.html.twig', [
            'libraires' => $libraireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="libraire_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $libraire = new Libraire();
        $form = $this->createForm(Libraire1Type::class, $libraire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($libraire);
            $entityManager->flush();

            return $this->redirectToRoute('libraire_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('libraire/new.html.twig', [
            'libraire' => $libraire,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="libraire_show", methods={"GET"})
     */
    public function show(Libraire $libraire): Response
    {
        return $this->render('libraire/show.html.twig', [
            'libraire' => $libraire,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="libraire_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Libraire $libraire): Response
    {
        $form = $this->createForm(Libraire1Type::class, $libraire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('libraire_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('libraire/edit.html.twig', [
            'libraire' => $libraire,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="libraire_delete", methods={"POST"})
     */
    public function delete(Request $request, Libraire $libraire): Response
    {
        if ($this->isCsrfTokenValid('delete'.$libraire->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($libraire);
            $entityManager->flush();
        }

        return $this->redirectToRoute('libraire_index', [], Response::HTTP_SEE_OTHER);
    }
}
