<?php

namespace App\Controller;

use App\Entity\Clients;
use App\Entity\Commande;
use App\Repository\ClientsRepository;
use App\Repository\CommandeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

     /**
     * @Route("/commander/livre/{id}",name="commander")
     */
    public function commander(Commande $commande)
    {
       // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $commande = new Commande();
        $commande->setNomClient($commande);
        $commande->setNomLibraire($commande);
        $commande->setDate(new \DateTime('now'));

        $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($commande);
            $entityManager->flush($commande);
            return $this->redirectToRoute('index');
    }
}
