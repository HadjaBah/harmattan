<?php

namespace App\Controller;

use App\Entity\OrgExt;
use App\Form\OrgExtType;
use App\Repository\OrgExtRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/org/ext")
 */
class OrgExtController extends AbstractController
{
    /**
     * @Route("/", name="org_ext_index", methods={"GET"})
     */
    public function index(OrgExtRepository $orgExtRepository): Response
    {
        return $this->render('org_ext/index.html.twig', [
            'org_exts' => $orgExtRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="org_ext_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $orgExt = new OrgExt();
        $form = $this->createForm(OrgExtType::class, $orgExt);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($orgExt);
            $entityManager->flush();

            return $this->redirectToRoute('org_ext_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('org_ext/new.html.twig', [
            'org_ext' => $orgExt,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="org_ext_show", methods={"GET"})
     */
    public function show(OrgExt $orgExt): Response
    {
        return $this->render('org_ext/show.html.twig', [
            'org_ext' => $orgExt,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="org_ext_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OrgExt $orgExt): Response
    {
        $form = $this->createForm(OrgExtType::class, $orgExt);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('org_ext_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('org_ext/edit.html.twig', [
            'org_ext' => $orgExt,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="org_ext_delete", methods={"POST"})
     */
    public function delete(Request $request, OrgExt $orgExt): Response
    {
        if ($this->isCsrfTokenValid('delete'.$orgExt->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($orgExt);
            $entityManager->flush();
        }

        return $this->redirectToRoute('org_ext_index', [], Response::HTTP_SEE_OTHER);
    }
}
