<?php

namespace App\Entity;

use App\Repository\CommandeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandeRepository::class)
 */
class Commande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dateCommande;

    /**
     * @ORM\ManyToOne(targetEntity=Clients::class, inversedBy="commandes")
     */
    private $nomClient;

    /**
     * @ORM\ManyToOne(targetEntity=Libraire::class, inversedBy="commandes")
     */
    private $nomLibraire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getDateCommande(): ?string
    {
        return $this->dateCommande;
    }

    public function setDateCommande(string $dateCommande): self
    {
        $this->dateCommande = $dateCommande;

        return $this;
    }

    public function getNomClient(): ?Clients
    {
        return $this->nomClient;
    }

    public function setNomClient(?Clients $nomClient): self
    {
        $this->nomClient = $nomClient;

        return $this;
    }

    public function getNomLibraire(): ?Libraire
    {
        return $this->nomLibraire;
    }

    public function setNomLibraire(?Libraire $nomLibraire): self
    {
        $this->nomLibraire = $nomLibraire;

        return $this;
    }
    
}
