<?php

namespace App\Entity;

use App\Repository\LivresRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LivresRepository::class)
 */
class Livres
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $datePub;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity=Clients::class, inversedBy="livres")
     */
    private $emailClient;

    /**
     * @ORM\ManyToOne(targetEntity=OrgExt::class, inversedBy="livres")
     */
    private $nomOrgExt;

    /**
     * @ORM\ManyToOne(targetEntity=Presse::class, inversedBy="livres")
     */
    private $nomPresse;

    /**
     * @ORM\ManyToOne(targetEntity=Etudiant::class, inversedBy="livres")
     */
    private $nomEtudiant;

    /**
     * @ORM\ManyToOne(targetEntity=Libraire::class, inversedBy="livres")
     */
    private $nomLibraire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDatePub(): ?string
    {
        return $this->datePub;
    }

    public function setDatePub(string $datePub): self
    {
        $this->datePub = $datePub;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getEmailClient(): ?Clients
    {
        return $this->emailClient;
    }

    public function setEmailClient(?Clients $emailClient): self
    {
        $this->emailClient = $emailClient;

        return $this;
    }

    public function getNomOrgExt(): ?OrgExt
    {
        return $this->nomOrgExt;
    }

    public function setNomOrgExt(?OrgExt $nomOrgExt): self
    {
        $this->nomOrgExt = $nomOrgExt;

        return $this;
    }

    public function getNomPresse(): ?Presse
    {
        return $this->nomPresse;
    }

    public function setNomPresse(?Presse $nomPresse): self
    {
        $this->nomPresse = $nomPresse;

        return $this;
    }

    public function getNomEtudiant(): ?Etudiant
    {
        return $this->nomEtudiant;
    }

    public function setNomEtudiant(?Etudiant $nomEtudiant): self
    {
        $this->nomEtudiant = $nomEtudiant;

        return $this;
    }

    public function getNomLibraire(): ?Libraire
    {
        return $this->nomLibraire;
    }

    public function setNomLibraire(?Libraire $nomLibraire): self
    {
        $this->nomLibraire = $nomLibraire;

        return $this;
    }
}
